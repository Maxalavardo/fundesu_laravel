<?php

namespace App\Http\Controllers;

use App\Http\Requests\locationFilter;
use App\Models\State;
use App\Http\Requests\StoreStateRequest;
use App\Http\Requests\UpdateStateRequest;

class StateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(locationFilter $request)
    {
        //
        if($request->state && $request->municipality) {
            $state = State::select()
                    ->join('municipalities', 'states.id', '=', 'municipalities.state_id')
                    ->join('parishes', 'municipalities.id', '=', 'parishes.municipality_id')
                    ->where('states.id', '=', $request->state)
                    ->Where('municipalities.id', '=', $request->municipality)
                    ->get();
        }else if($request->state) {
            $state = State::select()
                    ->join('municipalities', 'states.id', '=', 'municipalities.state_id')
                    ->where('states.id', '=', $request->state)
                    ->get();
        }else {
            $state = State::all();
        }

        return response()->json($state);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreStateRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreStateRequest $request)
    {
        //
        /* $request->validated();

        $state = new State();
        $state->name_state = $request->input('name_state');

        $res = $state->save();

        if ($res) {
            return response()->json(['message' => 'Post create succesfully'], 201);
        }
        return response()->json(['message' => 'Error to create post'], 500); */

        $state = State::create($request->validated());

        if ($state) {
            return response()->json(['data' => $state, 'message' => 'State create succesfully'], 201);
        }
        return response()->json(['message' => 'Error to create State'], 500);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\State  $state
     * @return \Illuminate\Http\Response
     */
    public function show(locationFilter $request)
    {
        //
        $stateShow = State::findOrFail($request->state);
        return response()->json($stateShow);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateStateRequest  $request
     * @param  \App\Models\State  $state
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateStateRequest $request, $id)
    {
        //
        $stateUp = State::findOrFail($id);
        $stateUp->update($request->validated());

        if ($stateUp) {
            return response()->json(['data' => $stateUp, 'message' => 'State update succesfully'], 201);
        }
        return response()->json(['message' => 'Error to update State'], 500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\State  $state
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $stateDestroy = State::destroy($id);

        return response()->json(['message' => 'State delete'], 200);
    }
}
