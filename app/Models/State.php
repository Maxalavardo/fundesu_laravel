<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class State extends Model
{
    use HasFactory;

    protected $guarded = [];

    /* protected $with = [
        'municipality',
        'municipality.parish'
    ]; */

    public function municipality() 
    {
        return $this->hasMany(Municipality::class);
    }
}
