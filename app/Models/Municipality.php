<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Municipality extends Model
{
    use HasFactory;

    public function parish()
    {
        return $this->hasMany(Parish::class);
    }

    public function state()
    {
        return $this->belongsTo(State::class);
    }
}
