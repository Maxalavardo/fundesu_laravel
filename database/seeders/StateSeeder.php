<?php

namespace Database\Seeders;

use App\Models\Municipality;
use App\Models\Parish;
use App\Models\State;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;

class StateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('states')->delete();
        $json = File::get('database/data/venezuela.json');
        $data = json_decode($json);

        foreach($data as $state)
        {
            State::create(array(
                'name_state' => $state->estado,
            ));

            foreach($state->municipios as $municipality)
            {
                $mun = Municipality::create(array(
                    'state_id' => $state->id_estado,
                    'name_municipality' => $municipality->municipio,
                ));

                foreach($municipality->parroquias as $parish)
                {
                    Parish::create(array(
                        'municipality_id' => $mun->id,
                        'name_parish' => $parish,
                    ));
                }
            }
        }
    }
}
