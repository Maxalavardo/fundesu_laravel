<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('states', function (Blueprint $table) {
            $table->id();
            $table->string('name_state');
            $table->timestamps();
        });

        Schema::create('municipalities', function (Blueprint $table) {
            $table->id();
            $table->string('name_municipality');
            $table->timestamps();
            // ----------------------
            $table->unsignedBigInteger('state_id');
            // ----------------------
            $table->foreign('state_id')->references('id')->on('states');
        });

        Schema::create('parishes', function (Blueprint $table) {
            $table->id();
            $table->string('name_parish');
            $table->timestamps();
            // ----------------------
            $table->unsignedBigInteger('municipality_id');
            // ----------------------
            $table->foreign('municipality_id')->references('id')->on('municipalities');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('states');
    }
}
